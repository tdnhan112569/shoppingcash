var data = 
    [{
        id:1,
      name:'Quần kaki KO1',
      uri:'https://cdn3.yame.vn/pimg/quan-kaki-nam-ma-bu-dai-l05-0015904/62f6c8be-d0b1-2400-bd3b-0014377106d0.jpg?w=440',
      price:'500.000',
      type:'Jeggings',
      saleTurnover: 60
    },
    {id:2,
        name:'Quần Jean JO1',
        uri:'https://cdn2.stylecraze.com/wp-content/uploads/2017/06/8-Boyfriend-Jeans.jpg',
        price:'350.000',
        type:'Straight Leg Jeans',
        saleTurnover: 90,
    },
    {id:3,
        name:'Quần Jean JO2',
        uri:'https://cdn2.stylecraze.com/wp-content/uploads/2017/06/5-Boot-Cut-700.jpg',
        price:'300.000',
        type:'Skinny',
        saleTurnover: 300
    },
    {id:4,
        name:'Quần Jean JO3',
        uri:'https://cdn2.stylecraze.com/wp-content/uploads/2017/06/9-Cigarette-Jeans-700.jpg',
        price:'370.000',
        type:'Boot Cut',
        saleTurnover: 100
    },
    {id:5,
        name:'Quần Jean2 J04',
        uri:'https://cdn2.stylecraze.com/wp-content/uploads/2017/06/7-Low-Rise-Jeans-700.jpg',
        price:'600.000',
        type:'Low Rise',
        saleTurnover: 279
    },
    {id:6,
        name:'H&M cuban',
        uri:'https://www.jcrew.com/brand_creative/fitguides/201809/m_shirts/201809_Sep2_M_ShirtFit_imgDefault.jpg',
        price:'600.000',
        type:'Cuban Collar Short Sleeve Shirt',
        saleTurnover: 600
    },
    {id:7,
        name:'Overshirt',
        uri:'http://ik.imagekit.io/bfrs/tr:w-600,h-600,pr-true,cm-pad_resize,bg-FFFFFF/image_vpenterprises/data/26-1.jpg',
        price:'600.000',
        type:'Overshirt',
        saleTurnover: 40
    },
    {id:8,
        name:'The humble flannel shirt',
        uri:'https://image.dhgate.com/0x0/f2/albu/g1/M00/79/D9/rBVaGFX2ckqAIpnFAAKcUzvvBko836.jpg',
        price:'600.000',
        type:'Flannel Shirt',
        saleTurnover: 60
    },
    {id:9,
        name:'SHIRT WITH CLASSIC COLLAR',
        uri:'https://5.imimg.com/data5/KN/BI/MY-26510312/formal-shirt-500x500.jpg',
        price:'600.000',
        type:'Office Shirt',
        saleTurnover: 700
    },
    {id:10,
        name:'PAUL SMITH Embroidered',
        uri:'https://thumbs2.ebaystatic.com/d/l225/m/mMpyj1dBOK7dXxFWNhnFcBw.jpg',
        price:'600.000',
        type:'Chambray',
        saleTurnover: 60
    }
    

    ];


export default data;