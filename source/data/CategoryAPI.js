import {categoryURL} from '../constants/URL'

async function getListCategorys() {
    try {
        let response = await fetch(
            categoryURL,
        );
        let responseJson = await response.json();
        return responseJson.body;
        } catch (error) {
        console.error(error);
        }
}

export {getListCategorys}