 
 const BaseUrl = 'http://api.demo.nordiccoder.com/api'
 
 const productURL = BaseUrl + '/products'
 const categoryURL =BaseUrl + '/categories'

export default {productURL, categoryURL}
