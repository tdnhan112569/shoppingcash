import React from 'react';
import {Text, View, Image, Dimensions } from 'react-native';
var {height, width} = Dimensions.get('screen');

export default class Banner extends React.Component {
    render() {
      return (
        <Image 
            style={{width: width, height:50}}
            resizeMode='repeat'
            source={{uri:'https://americanpomade.vn/wp-content/uploads/2017/02/Sale-Banner-1920-x-600-FINAL.jpg'}}
        />
      );
    }
  }