import React from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import {bodyStyle} from '../../constants/styles'


var {height, width} = Dimensions.get('screen');

export default class ItemCategory extends React.Component {
    render() {
      const {item} = this.props
      return (
        <View style={styles.itemContainer}>
            <Text style={styles.itemText}>{item.type}</Text>
        </View>
    );
  }
}


const styles = StyleSheet.create({
  itemContainer:{
    flex: 1, borderRadius:20, width:'100%' , backgroundColor: 'steelblue', marginLeft:10, justifyContent:'center', alignItems:'center'
  },
  itemText:{
    fontSize:15, color:'white', padding: 10,
  }
})