import React from 'react';
import { StyleSheet, Text, View, FlatList, Dimensions, ScrollView } from 'react-native';
import {bodyStyle} from '../../constants/styles'
import data from '../../data/fakeData'
import {getListCategorys} from '../../data/CategoryAPI'
import ItemProduct from './ItemProduct'
import ItemCategory from './ItemCategory'
import { CheckBox } from 'react-native-elements'

var {height, width} = Dimensions.get('screen');

export default class App extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
          isCompleted : false,
          listCategory: [],
          isChecked: false,
          listProduct: data
        }
    }

    // getDataCategoryFromServer = () =>{
    //   getListCategorys().then((result) =>{
    //      this.setState({listCategory: result})
    //   }).catch((error) =>{
    //      this.state({listCategory:[]})
    //      console.log(error)
    //   })
    // }

   componentDidMount(){
    // this.getDataCategoryFromServer()
   }

   filterListProduct = () => {

      if(this.state.isChecked == false) {
        let arrTopFive = []
        let arrTemp = data.slice()
        let index = 0

        arrTemp.sort((product1, product2)=>{
          if(product1.saleTurnover > product2.saleTurnover) return -1
          if(product1.saleTurnover <  product2.saleTurnover) return 1
          return 0
        })
        
        while(arrTopFive.length < 5) {
          arrTopFive.push(arrTemp[index])
          index++
        }

        this.setState({isChecked:!this.state.isChecked, listProduct:arrTopFive })
      }
      else {
        this.setState({isChecked:!this.state.isChecked, listProduct : data })
      }
    
      
   }
  
    render() {
      const type = true;
      return (
        
        <ScrollView>
          <View style={bodyStyle.body}>

              <View style={{height:30, width: width, marginTop:10, marginRight:5 }}>  
                  <FlatList 
                    data={data}
                    renderItem={({item}) => <ItemCategory item ={item}/>}
                    horizontal={type}
                    keyExtractor={(item, index) => item.id + " " + 'OKC'}
                    style={{flex:1 ,width:width, paddingBottom:10}}
                  />
              </View>
              <CheckBox
                title='Top 5 product best buy'
                checkedColor='green'
                checked={this.state.isChecked}
                onPress={this.filterListProduct}
              />
              
                <FlatList
                  data={this.state.listProduct}
                  renderItem={({item}) => <ItemProduct item={item} />}
                  keyExtractor={(item, index) => item.id + " " + index}
                  style={{flex:1, width:width}}
                />
              
          </View>
        </ScrollView>
    );
  }
}
  