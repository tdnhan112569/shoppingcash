import React from 'react';
import { StyleSheet, Text, View , Image, Button, Dimensions, TouchableOpacity} from 'react-native';

var {height, width} = Dimensions.get('window');

const char = `<>`

export default class ItemProduct extends React.Component {
    render() {
      const {item} = this.props
      return (
        <View style={styles.containerItem}> 
            <Text style={styles.itemName}>{item.name}</Text>
            <Image 
                style={styles.itemImage}
                source={{uri: item.uri }}
                resizeMode='cover'
            />
            <Text style={styles.itemType}>{item.type}</Text>
            <Text style={styles.saleOffPrice}>{item.price * 0.8 }.000$</Text>
            <Text style={styles.price}>{item.price}$</Text>
            <TouchableOpacity style={styles.buttonViewNow}>
                 <Text style={styles.textViewNow}>VIEW NOW</Text>
            </TouchableOpacity>
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    containerItem:{
      margin:10, 
      alignItems:'center',
      justifyContent:'center', 
      borderColor:'white',
      borderRadius:0.5,
      shadowColor:'gray', shadowOpacity:0.5,
      shadowOffset:{  width: 10,  height: 0  },
      shadowRadius: 2,
      elevation: 3
    },
    itemName: {
      fontSize:25, 
      marginBottom: 10, 
      color: 'rgb(67, 72, 77)', 
      fontWeight: "bold", 
      marginTop: 20, 
      textAlign:'center'
    },
    itemImage: {
      height:height /4, width:width * 0.7, marginBottom:10, borderRadius:0.5
    },
    itemType:{
      fontSize:20, marginBottom: 5, color:'black'
    },
    saleOffPrice:{
      fontSize:30, marginBottom: 5, color:'red', fontWeight: "bold"
    },
    price:{
      fontSize:20, marginBottom: 5, color:'gray', textDecorationLine:'line-through'
    },
    buttonViewNow:{
      height:50, width:width * 0.7, alignItems:'center', justifyContent:'center',backgroundColor:'red', marginBottom: 20 
    },
    textViewNow:{
      fontSize:20, fontWeight: "bold", color:'white' 
    }
}); 